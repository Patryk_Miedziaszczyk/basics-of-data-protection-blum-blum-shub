import sympy
import math
import random
import re
def generatorPrimeNumber():
   primeNumbers = []
   p = 0
   while(p%4!=3):
       p = sympy.randprime(0, 10000000)
       if(p%4==3):
           break
   primeNumbers.append(p)
   q = 0
   while (q % 4 != 3):
       q = sympy.randprime(0, 10000000)
       if (q % 4 == 3):
           break
   primeNumbers.append(q)
   return primeNumbers

def originalValue():
   data = []
   primeNumbers = generatorPrimeNumber()
   N = primeNumbers[0] * primeNumbers[1]
   x = 0
   while (math.gcd(x,N)!=1):
       x = random.randint(0,10000000)
   originalValue = (x*x)%N
   data.append(N)
   data.append(originalValue)
   return data

def keyGenerator():
   global keyValue
   data = originalValue()
   N = data[0]
   x = data[1]
   iterator = 1
   key = []
   while (iterator != 20000):
       if (x % 2 == 0):
           keyValue = 0
       elif (x % 2 == 1):
           keyValue = 1
       key.append(keyValue)
       x = x*x % N
       iterator += 1
   return key

def singleBitsTest(key):
   numberOfOnes = 0
   iterator = 0
   while (iterator != 19999):
       if (key[iterator]==1):
           numberOfOnes += 1
       iterator += 1
   if(9725 < numberOfOnes <10275):
       print("Test serii: wynik pozytywny")
   else:
       print("Test serii: wynik negatywny")

def counter(array):
   counterArray = [0,0,0,0,0,0]
   value = array[0]
   iterator = 0
   counter = 0
   while iterator != len(array)-1:
       if(array[iterator]==0):
           value = 0
           counter += 1
           iterator += 1
       elif(array[iterator] == 1 and value == 0):
           value = 1
           if (counter == 1):
               counterArray[0] += 1
           elif (counter == 2):
               counterArray[1] += 1
           elif (counter == 3):
               counterArray[2] += 1
           elif (counter == 4):
               counterArray[3] += 1
           elif (counter == 5):
               counterArray[4] += 1
           elif (counter == 6 or counter > 6):
               counterArray[5] += 1
           counter = 0
       elif (array[iterator] == 1 and value == 1):
           iterator += 1
   return counterArray

def seriesTest(key):
   counterArray = counter(key)
   if(2315 < counterArray[0] < 2685 and 1114 < counterArray[1] < 1386 and 527 < counterArray[2] < 723 and
           240 < counterArray[3] < 384 and 103 < counterArray[4] < 209 and 103 < counterArray[5] < 209):
       print("Test serii: wynik pozytywny")
   else:
       print("Test serii: wynik negatywny")

def longSeriesTest(key):
   key = ''.join([str(element) for element in key])
   x = re.findall("0{26}0*|1{26}1*", key)
   if (len(x)==0):
       print("Test serii: wynik pozytywny")
   else:
       print("Test serii: wynik negatywny")

def testPokerowy(key):
   key = ''.join([str(elem) for elem in key])
   array = re.findall('....', key)
   numberOfOccurrences = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
   iterator = 0
   while(iterator != len(array)-1):
       if(array[iterator] == "0000"):
           numberOfOccurrences[0] +=1
           iterator += 1
       elif (array[iterator] == "0001"):
           numberOfOccurrences[1] += 1
           iterator += 1
       elif (array[iterator] == "0010"):
           numberOfOccurrences[2] += 1
           iterator += 1
       elif (array[iterator] == "0011"):
           numberOfOccurrences[3] += 1
           iterator += 1
       elif (array[iterator] == "0100"):
           numberOfOccurrences[4] += 1
           iterator += 1
       elif (array[iterator] == "0101"):
           numberOfOccurrences[5] += 1
           iterator += 1
       elif (array[iterator] == "0110"):
           numberOfOccurrences[6] += 1
           iterator += 1
       elif (array[iterator] == "0111"):
           numberOfOccurrences[7] += 1
           iterator += 1
       elif (array[iterator] == "1000"):
           numberOfOccurrences[8] += 1
           iterator += 1
       elif (array[iterator] == "1001"):
           numberOfOccurrences[9] += 1
           iterator += 1
       elif (array[iterator] == "1010"):
           numberOfOccurrences[10] += 1
           iterator += 1
       elif (array[iterator] == "1011"):
           numberOfOccurrences[11] += 1
           iterator += 1
       elif (array[iterator] == "1100"):
           numberOfOccurrences[12] += 1
           iterator += 1
       elif (array[iterator] == "1101"):
           numberOfOccurrences[13] += 1
           iterator += 1
       elif (array[iterator] == "1110"):
           numberOfOccurrences[14] += 1
           iterator += 1
       elif (array[iterator] == "1111"):
           numberOfOccurrences[15] += 1
           iterator += 1
   sum = 0
   for x in numberOfOccurrences:
       sum = sum + (x*x)
   X = 16 / 5000 * sum - 5000
   if (2,16 < X < 46,17):
       print("Test serii: wynik pozytywny")
   else:
       print("Test serii: wynik negatywny")

testPokerowy(keyGenerator())
singleBitsTest(keyGenerator())
seriesTest(keyGenerator())
longSeriesTest(keyGenerator())


